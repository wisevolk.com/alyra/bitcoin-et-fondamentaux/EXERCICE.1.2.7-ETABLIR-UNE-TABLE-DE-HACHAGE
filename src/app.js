const chalk = require("chalk");
const crypto = require("crypto");

const hashTable = new Map();

class Site {
    constructor(_ville, _ip) {
        this.ville = _ville;
        this.ip = _ip;
    }

    keyToHash() {
        console.log(this.ville + this.ip.substring(0, this.ip.indexOf(".")));
        return this.ville + this.ip.substring(0, this.ip.indexOf("."));
    }
}

function hashKeyEncoder(keyToHash){
    return  crypto.createHash("md5", {salt: "monGrainDeSel"}).update(keyToHash).digest("hex").substring(0,8);
}

function createHashTable(_lieu, _serveur) {
    const site = new Site(_lieu, _serveur);
    const key = hashKeyEncoder(site.keyToHash());
    hashTable.set(key, site);
}

function getSite(site, ip){
    return hashTable.get(hashKeyEncoder(site+ip));
}


createHashTable("Amsterdam", "153.8.223.72");
createHashTable("Chennai","169.38.84.49");
createHashTable("Dallas","169.46..49.112");
createHashTable("Dallas, TX, USA", "184.173.213.155");
createHashTable("Frankfurt","159.122.100.41");
createHashTable("Hong Kong","119.81.134.212");
createHashTable("London","5.10.5.200");
createHashTable("London","158.176.81.249");
createHashTable("Melbourne","168.1.168.251");
createHashTable("Mexico City","169.57.7.230");
createHashTable("Milan","159.122.142.111");
createHashTable("Paris","158.8.78.42");
createHashTable("San Jose","192.155.217.197");
createHashTable("Säo Paulo","169.57.153.228");
createHashTable("Toronto","169.56.184.72");
createHashTable("Washington DC","50.87.60.166");

console.log(hashTable);
console.log(hashTable.size);

console.log(getSite("London", "158"));
